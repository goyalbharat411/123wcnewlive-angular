import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductCategoryComponent } from './product-category/product-category.component';

const routes: Routes = [
{ path: '', component: HomeComponent },
{ path: 'product-category', component: ProductCategoryComponent }

];

export const AppRoutingModule = RouterModule.forRoot(routes); { }
